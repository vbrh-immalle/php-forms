
<p>
      Geef b.v. volgende code in:
      <ul>
          <li>
              <code>
                &lt;pre&gt;test&lt;/pre&gt;
              </code>
          </li>
          <li>
              <code>
                &lt;script&gt;alert("Hello");&lt;/script&gt;
              </code>
          </li>
    </ul>
</p>

<form method="post">
    <p>
    Probeer hier javascript- of html-code in te geven:
    </p>
    <input type="textarea" name="unescapedCode">
    <input type="submit">
</form>


<form method="post">
    <p>
    Probeer hier javascript- of html-code in te geven:
    </p>
    <input type="textarea" name="escapedCode">
    <input type="submit">
</form>

<?php

    if ($_SERVER["REQUEST_METHOD"] == "POST") {

        echo "<br>";
        echo "Dit is een POST-request.";
        echo "<br>";
        echo "We tonen de aanwezige data:";

        echo "<p>We tonen nu de unescaped code:<br> ";
        echo $_POST["unescapedCode"];
        echo "</p>";

        echo "<p>We tonen nu de escaped code:<br>";
        echo htmlspecialchars($_POST["escapedCode"]);
        echo "</p>";

    } else {

        /* Voeg hier output toe, die de gebruiker meldt dat
         * we nu nog geen POST-data hebben en hij iets moet
         * ingeven in 1 van bovenstaande formulieren.
         */

        echo "<br>";
        echo "Dit is geen POST-request, vandaar deze boodschap...";
        echo "<br>";
        echo "Geef wat test-data in in 1 van bovenstaande formulieren.";
        echo "<br>";
        echo "Zorg dat je begrijpt wat <i>escapen</i> inhoudt.";

    }

    /* Vraagjes:
     *
     * - Als we op elke submit-button drukken, worden dan alle ingevulde waarden meegegeven
     *   of enkel die van het betreffende formulier?
     * - Wat verstaan we onder het *escapen* van html-code?
     * - Lukt het je om javascript code uit te voeren via het invulformulier?
     *
     */
?>

