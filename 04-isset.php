
<?php

    /*
     * Zorg dat als deze file wordt aangeroepen met url-parameter `info`,
     * deze wordt getoond. (b.v. `http://localhost/php-forms/04-isset.php?info=interessant`)
     * ALs `info` niet gezet is (b.v. `http://localhost/php-forms/04-isset.php`),
     * moet een foutmelding gegeven worden.
     */

    if( isset($_GET['info']) ) {
        echo "De info is " . $_GET['info'];
    } else {
        echo "Er werd geen info meegegeven via een URL-parameter!";
    }


?>
