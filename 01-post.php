

<!-- pas formulier en php-code zo aan dat een POST wordt gebruikt ipv GET -->
<!-- zorg ook dat alle ingegeven informatie wordt afgedrukt -->


<form method="post">
    <p>
        Nickname: <input type="text" name="naam">
    </p>
    <p>
        Geslacht:
        <input type="radio" name="geslacht" value="mannelijk">Mannelijk</input>
        <input type="radio" name="geslacht" value="vrouwelijk">Vrouwelijk</input>
    </p>
    <input type="submit">
</form>

<ul>
<?php


echo "<li>" . $_POST["naam"] . "</li>";
echo "<li>" . $_POST["geslacht"] . "</li>";


/*
 * Wanneer GET of POST gebruiken?
 *
 * GET wordt gebruikt wanneer gegevens opgevraagd moeten worden.
 * Een GET-request mag niet zorgen voor een verandering op de server
 * of in de database.
 *
 * POST wordt gebruikt om nieuwe data op de server (of database)
 * op te slaan of om een wijziging aan te brengen.
 *
 * Hoewel POST-variabelen niet zichtbaar zijn in de URL,
 * kan je ze wel nog gemakkelijk opsporen met de juiste tools,
 * omdat ze cleartext in de HTTP-header staan.
 * Extra security krijg je dus eigenlijk NIET met een POST.
 */

?>
</ul>
