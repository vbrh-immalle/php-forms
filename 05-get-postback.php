<!-- Waarden "onthouden" bij een POSTBACK -->

<!--
     Zorg dat je goed begrijp wat een POSTBACK is, nl.
     het opnieuw openen van dezelfde pagina maar ditmaal met GET- of POST-data.
     (Misschien moeten we spreken over GETBACK en POSTBACK...)
-->

<form method="get">
    <select name="vorm">
        <option value="ovaal">ovale vorm</option>
        <option value="rond">ronde vorm</option>
        <option value="vierkant">vierkante vorm</option>
    </select>
    <select name="kleur">
        <option value="rood">rode kleur</option>
        <option value="blauw">blauw kleur</option>
        <option value="oranje">oranje kleur</option>
    </select>
    <input type="submit">
</form>

<script>
    /* Javascript API voor select box: 
     * 
     * e = document.getElementByName("vorm")[0];
     * e.selectedIndex = 0; // of
     * e.value = "vierkant";
     *
     */

    document.getElementsByName("vorm")[0].value = "<?php echo $_GET['vorm']; ?>";
    document.getElementsByName("kleur")[0].value = "<?php echo $_GET['kleur']; ?>";
</script>


<ul>
<?php
// We tonen de inhoud v.h. GET-request:

echo "<li>" . $_GET['vorm'] . "</li>";
echo "<li>" . $_GET['kleur'] . "</li>";

?>
</ul>
